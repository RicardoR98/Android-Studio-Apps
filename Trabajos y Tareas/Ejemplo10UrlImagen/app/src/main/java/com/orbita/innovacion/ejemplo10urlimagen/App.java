package com.orbita.innovacion.ejemplo10urlimagen;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.TimerTask;

public class App extends AppCompatActivity implements View.OnClickListener {

    private Spinner spinner;
    private ImageView imageView;
    private Button sd, internal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imagen);

        spinner = (Spinner) findViewById(R.id.spinner);

        imageView = (ImageView) findViewById(R.id.imageView);

        sd = (Button) findViewById(R.id.btnSd);
        internal = (Button) findViewById(R.id.btninterna);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String string = (String) parent.getItemAtPosition(position);
                download dw = new download();
                dw.execute(string);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btninterna){
            //implementacion memoria interna
            Bitmap image = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            String ruta = saveInterna(this, getTitle().toString(), image);
            Toast.makeText(this, ruta, Toast.LENGTH_SHORT).show();
        }else if(v.getId() == R.id.btnSd){
            //implementacion memoria externa

        }
    }

    private String saveInterna(Context context, String name, Bitmap bitmap) {

        ContextWrapper contextWrapper = new ContextWrapper(context);
        File dirImages = contextWrapper.getDir("Imagenes", context.MODE_PRIVATE);
        File path = new File(dirImages, name+".jpg");
        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 10, fos);
            fos.flush();
        }catch (FileNotFoundException a){
            a.printStackTrace();
        }catch (IOException b){
            b.printStackTrace();
        }

        return path.getAbsolutePath();
    }

    class download extends AsyncTask<String, Void, Bitmap>{
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(App.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Descargando Imagen");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setProgress(0);
            progressDialog.setMax(100);
            progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            try{
                Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL(strings[0]).getContent());
                return bitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if(bitmap != null)
                imageView.setImageBitmap(bitmap);
            progressDialog.dismiss();
        }
    }

}
