package com.orbita.innovacion.ejemplo05actividades;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class App extends AppCompatActivity {

    private Button second;
    private static final int REQUEST_CAMERA = 0;
    public static final int REQUEST_CAMERA_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);
        second = (Button) findViewById(R.id.btmSAct);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App.this.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void second(View view) {
        Intent intent = new Intent(this, Second.class);
        startActivity(intent);
    }

    public void tercera(View view) {
        Intent intent = new Intent(this, DataAct.class);
        intent.putExtra("name", "Ricardo");
        startActivityForResult(intent, 10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 10){
            String ok = data.getStringExtra("result");
            Toast.makeText(this, ok, Toast.LENGTH_SHORT).show();
        }

    }

    private void tomolafoto(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivity(intent);
        Toast.makeText(this, "Gracias", Toast.LENGTH_SHORT).show();
    }
    public void camara(View view) {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            tomolafoto();
            Log.d("Permiso","Camara: Menor que android 6");
        }else{
            int permiso = checkSelfPermission(Manifest.permission.CAMERA);
            if(permiso == PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.CAMERA},001);
                //tomolafoto();
                Log.d("Permiso","Camara: Mayor que android 6");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CAMERA:
                final int numOfRequest = grantResults.length;
                final boolean isGranted = numOfRequest == 1 && PackageManager.PERMISSION_GRANTED == grantResults[numOfRequest - 1];
                Log.v("Error", "Camera permission callback on onRequestpermission");
                if(isGranted){
                    tomolafoto();
                }
                break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
