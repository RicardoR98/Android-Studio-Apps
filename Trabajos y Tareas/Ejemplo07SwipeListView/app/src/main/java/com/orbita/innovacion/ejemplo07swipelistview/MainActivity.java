package com.orbita.innovacion.ejemplo07swipelistview;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipe = null;
    private int cuantos = 10;
    private ArrayAdapter<String> adapter = null;
    private ArrayList<String> names = null;
    private ListView lista = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe);

        names = new ArrayList<>();

        names.add("Ricar2");
        names.add("Kike");
        names.add("Angela");
        names.add("Yarely");
        names.add("Angelica");
        names.add("Chardo");
        names.add("Jalil");
        names.add("Che Portillo");
        names.add("Gustavo San");
        names.add("Eduardo");

        //Obtenemos el Layout para modificarlo
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipe.setOnRefreshListener(this);
        swipe.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_dark,
                android.R.color.holo_purple,
                android.R.color.holo_red_light);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        lista = (ListView) findViewById(R.id.List);
        lista.setAdapter(adapter);

        lista.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // Estamos en el elemento superior
                int filasuperior = (lista == null || lista.getChildCount() == 0 ) ? 0 : lista.getChildAt(0).getTop();
                // si no hay elementos deshanilitamos la lista para no genenrar el evento scrool
                swipe.setEnabled(filasuperior >= 0);
            }
        });

    }

    @Override
    public void onRefresh() {
        // Que comienze el juego girando
        swipe.setRefreshing(true);
        // vamos a generar un hilo para simular la carga de un dato y modificar en runtime el listview
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(lista.getChildCount() > 0)
                names.remove(--cuantos);
                adapter.notifyDataSetChanged();
                swipe.setRefreshing(false);

                //adapter.clear(); // limpiamos el listview queda vacio

                // rellenar de nuevo el adapter con los nuevos datos
            }
        }, 100);
    }

}
