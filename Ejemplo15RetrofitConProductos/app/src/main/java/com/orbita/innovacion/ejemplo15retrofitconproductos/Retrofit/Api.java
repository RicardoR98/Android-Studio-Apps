package com.orbita.innovacion.ejemplo15retrofitconproductos.Retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {

    @GET("android/get_all_products.php")
    Call<JSONResponse> getJson();

    @FormUrlEncoded
    @POST("android/create_product.php")
    Call<ResponseBody> sendData(
    @Field("name") String Name,
    @Field("price") double Price,
    @Field("description") String Description);

    @FormUrlEncoded
    @POST("android/update_product.php")
    Call<ResponseBody> updateData(
            @Field("pid") int Pid,
            @Field("name") String Name,
            @Field("price") double Price,
            @Field("description") String Description);

    @FormUrlEncoded
    @POST("android/delete_product.php")
    Call<ResponseBody> deteleData(
            @Field("pid") int Pid);
}
