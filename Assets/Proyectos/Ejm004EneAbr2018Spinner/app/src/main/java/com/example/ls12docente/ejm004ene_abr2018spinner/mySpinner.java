package com.example.ls12docente.ejm004ene_abr2018spinner;

/**
 * Created by LS12DOCENTE on 17/01/2018.
 */
public class mySpinner {
    private int id;
    private String name;

    public mySpinner(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return    name;

    }
}
