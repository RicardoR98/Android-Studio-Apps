package com.orbita.innovacion.ejemplo08gridview;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class App extends AppCompatActivity {

    private GridView gridView = null;
    private itemAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        gridView = (GridView)findViewById(R.id.gridView);
        adapter = new itemAdapter(this,R.layout.row_grid, fillImages());
        gridView.setAdapter(adapter);
    }
    private ArrayList<item> fillImages(){
        int values =  Integer.parseInt(getResources().getString(R.string.howmanyimages));
        ArrayList<item>lista = new ArrayList<>();

        for(int i = 0; i <= values; i++ ) {

            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), getResources().getIdentifier(
                    "sample_" + i, "drawable", "com.orbita.innovacion.ejemplo08gridview"));
            lista.add(new item(bitmap,"sample_" + i));
        }
        return lista;
    }
}