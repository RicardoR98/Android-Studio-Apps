package com.orbita.innovacion.sva_escuela;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentEventos extends Fragment {

    private LottieAnimationView success, error;

    private String IDCorreo;
    private String foto, nombre, tema;

    private EditText nombree, mensaje, fecha;
    private Button publicar;

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_eventos, container, false);

        success = (LottieAnimationView) v.findViewById(R.id.success);
        error = (LottieAnimationView) v.findViewById(R.id.error);
        nombree = (EditText) v.findViewById(R.id.nombreEditText);
        mensaje = (EditText) v.findViewById(R.id.mensajeEditText);
        fecha = (EditText) v.findViewById(R.id.fechaEditText);
        publicar = (Button) v.findViewById(R.id.btnPublicar);

        IDCorreo = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("IDCorreo","null");

        DocumentReference miDocRef = FirebaseFirestore.getInstance().document("Personal/" + IDCorreo);


        miDocRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){

                    foto = documentSnapshot.getString("foto");
                    nombre = documentSnapshot.getString("nombre");
                    tema = documentSnapshot.getString("tema");

                    llenar();

                }else{
                }
            }
        });

        publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nombree.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo nombre vacío", Toast.LENGTH_SHORT).show();
                    return;
                }else if(mensaje.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo mensaje vacío", Toast.LENGTH_SHORT).show();
                    return;
                }else if(fecha.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo fecha vacío", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    subir();
                }
            }
        });

        fecha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    Calendario dialog = new Calendario(v);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    dialog.show(ft, "Date");
                }
            }
        });

        publicar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                // Crea el nuevo fragmento y la transacción.
                Fragment nuevoFragmento = new FragmentEventosVer();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fram, nuevoFragmento);
                transaction.addToBackStack(null);

                // Commit a la transacción
                transaction.commit();

                return false;
            }
        });

        return v;
    }

    private void subir() {
        String mens = mensaje.getText().toString();
        String fech = fecha.getText().toString();
        Map<String, Object> data = new HashMap<>();
        data.put("img_perfil", foto);
        data.put("mensaje", fech + " - " + mens);
        data.put("nombre", nombre);
        db.collection("Eventos").add(data).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                success.setVisibility(View.VISIBLE);
                success.playAnimation();
                mensaje.setText(null);
                fecha.setText(null);
                NotificarPhp NP = new NotificarPhp(getContext(), tema, nombre);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                error.setVisibility(View.VISIBLE);
                error.playAnimation();
            }
        });
    }

    private void llenar() {
        nombree.setText(nombre);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("Nombre");
        editor.putString("Nombre", nombre.toString());
        editor.apply();
    }

}
