package com.orbita.innovacion.sva_escuela;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private EditText correo ,contraseña;
    private TextInputLayout LCorreo, LContraseña;
    private Button LogIn, login;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener listener;
    private VideoView mVideoView;
    private ProgressBar progressBar;

    AlphaAnimation fadeOut;
    AlphaAnimation fadeIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= 21){
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        login = (Button) findViewById(R.id.Btn);
        mVideoView = (VideoView) findViewById(R.id.bgVideoView);

        video();

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(true);
            }
        });

        mAuth = FirebaseAuth.getInstance();
        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = mAuth.getCurrentUser();
                if(user != null){
                    goMainScreen();
                }
            }
        };

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.startAnimation(fadeIn);
                displayDialog();
                desaparecer();
            }
        });

        fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(1500);
        fadeIn.setStartOffset(2000);
        fadeIn.setFillAfter(true);

        fadeOut = new AlphaAnimation(1.0f, 0.0f);
        fadeOut.setDuration(1500);
        fadeOut.setStartOffset(2000);
        fadeOut.setFillAfter(true);

        mVideoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                login.startAnimation(fadeIn);

                desaparecer();

                return true;
            }
        });

        desaparecer();
    }

    private void desaparecer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                login.startAnimation(fadeOut);
            }
        }, 6000);
    }

    private void Ingresar(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    goMainScreen();
                }else{
                    progressBar.setVisibility(View.GONE);
                    correo.setVisibility(View.VISIBLE);
                    LCorreo.setVisibility(View.VISIBLE);
                    contraseña.setVisibility(View.VISIBLE);
                    LContraseña.setVisibility(View.VISIBLE);
                    LogIn.setVisibility(View.VISIBLE);

                    Toast.makeText(LoginActivity.this, "Correo o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void goMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(listener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(listener != null){
            mAuth.removeAuthStateListener(listener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        video();
    }

    private void video() {
        Uri uri = Uri.parse("https://drive.google.com/uc?export=download&id=1hsT1hJ9cxQ2JDO4-z14TBFgWRC6M6TN3");
        mVideoView.setVideoURI(uri);
        mVideoView.start();
    }

    private void displayDialog()
    {
        Dialog d = new Dialog(this);
        d.setContentView(R.layout.login);
        final LinearLayout myLogin;
        progressBar = (ProgressBar) d.findViewById(R.id.progressBar);
        myLogin = (LinearLayout) d.findViewById(R.id.MyLogIn);
        correo = (EditText) d.findViewById(R.id.correoEditText);
        LCorreo = (TextInputLayout) d.findViewById(R.id.correoLayout);
        contraseña = (EditText) d.findViewById(R.id.contraseñaEditText);
        LContraseña = (TextInputLayout) d.findViewById(R.id.contraseñaLayout);
        LogIn = (Button) d.findViewById(R.id.BtnLogIn);

        LogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(correo.getText().toString().equals("")){
                    Snackbar.make(myLogin, "Campo de Correo vacío",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                    return;
                }else if(contraseña.getText().toString().equals("")){
                    Snackbar.make(myLogin, "Campo de Contraseña vacío",
                            Snackbar.LENGTH_LONG).setAction("", null).show();
                    return;
                }else {
                    InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(contraseña.getWindowToken(), 0);

                    progressBar.setVisibility(View.VISIBLE);
                    correo.setVisibility(View.GONE);
                    LCorreo.setVisibility(View.GONE);
                    contraseña.setVisibility(View.GONE);
                    LContraseña.setVisibility(View.GONE);
                    LogIn.setVisibility(View.GONE);

                    String email = correo.getText().toString();
                    String password = contraseña.getText().toString();
                    Ingresar(email, password);
                }
            }
        });
        d.show();
    }

}
