package com.orbita.innovacion.proyinte;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase de Fragmento encargada de poder visualizar lo que estan preprando o tienen a la venta dentro
 * del comedor o cafeteria del plantel.
 *
 * Fecha de creación: 15/02/2018.
 * Versión: 18.2.15
 * Modificaciones:
 * Adaptacion de CardView y RecyclerView para la visualizacion de alimentos del menu del plantel 15/02/2018
 */

public class FragmentComedor extends Fragment {

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private ProgressBar progressBar;
    private RecyclerView.Adapter adapter;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    private ArrayList<Item_Comedor> items = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_comedor, container, false);

        items = new ArrayList<Item_Comedor>();

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        // Obtener el RecyclerView
        recycler = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(false);

        // Llamada a la coleccion de Comedor para traer todos los datos
        db.collection("Comedor").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new Item_Comedor(document.getString("nombre"),
                                        document.getString("descripcion"),
                                        document.getString("img")));
                            }

                            // Usar un administrador para LinearLayout
                            lManager = new LinearLayoutManager(getContext());
                            recycler.setLayoutManager(lManager);

                            // Crear un nuevo adaptador
                            adapter = new AdapterListItem(items);
                            recycler.setAdapter(adapter);

                            progressBar.setVisibility(View.GONE);
                            recycler.setVisibility(View.VISIBLE);
                        } else {

                        }
                    }
                });

        return v;
    }

    //Clase anidada para adaptar datos al CardView y RecyclerView
    public class AdapterListItem extends RecyclerView.Adapter<AdapterListItem.ComedorViewHolder> {
        //Creacion de lista de items
        private List<Item_Comedor> items;

        public class ComedorViewHolder extends RecyclerView.ViewHolder{
            // Campos respectivos de un item
            private ImageView imagen;
            private TextView nombre;
            private TextView descripcion;

            public ComedorViewHolder(View v) {
                //Inicilizando WidGets del CardView (item_comedor)
                super(v);
                imagen = (ImageView) v.findViewById(R.id.img);
                nombre = (TextView) v.findViewById(R.id.txtNombreCard);
                descripcion = (TextView) v.findViewById(R.id.txtDescripcionCard);
            }
        }

        public AdapterListItem(List<Item_Comedor> items) {
            //Igualando datos de la lista
            this.items = items;
        }

        @Override
        public int getItemCount() {
            //Sacando tamaño de la lista
            return items.size();
        }

        @Override
        public ComedorViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            //Inflando Layout de CardView (item_comedor)
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_comedor, viewGroup, false);
            return new ComedorViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ComedorViewHolder viewHolder, int i) {
            //Agregando los datos al CardView (item_comedor)
            viewHolder.imagen.setImageBitmap(items.get(i).getImg());
            viewHolder.nombre.setText(items.get(i).getNombre());
            viewHolder.descripcion.setText(items.get(i).getDescripcion());
        }
    }

}
