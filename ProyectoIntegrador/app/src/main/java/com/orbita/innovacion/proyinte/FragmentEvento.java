package com.orbita.innovacion.proyinte;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Clase de Fragmento encargada de poder visualizar los eventos que se tengan dentro de la institucion
 * para poderse visualizar en FragmentoComedor
 *
 * Fecha de creación: 17/02/2018.
 * Versión: 18.2.15
 * Modificaciones:
 * Adaptacion de CardView y RecyclerView para la visualizacion de eventos de la institucion 15/02/2018
 */

public class FragmentEvento extends Fragment {

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private ProgressBar progressBar;
    private RecyclerView.Adapter adapter;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    private ArrayList<Item_Evento> items = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_evento, container, false);

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        items = new ArrayList<Item_Evento>();

        // Obtener el RecyclerView
        recycler = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(false);

        // Llamada a la coleccion de Eventos para traer todos los datos
        db.collection("Eventos").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new Item_Evento(document.getString("nombre"),
                                        document.getString("mensaje"),
                                        document.getString("img_perfil")));
                            }

                            // Usar un administrador para LinearLayout
                            lManager = new LinearLayoutManager(getContext());
                            recycler.setLayoutManager(lManager);

                            // Crear un nuevo adaptador
                            adapter = new AdapterListItem(items);
                            recycler.setAdapter(adapter);

                            progressBar.setVisibility(View.GONE);
                            recycler.setVisibility(View.VISIBLE);
                        } else {

                        }
                    }
                });

        return v;
    }

    //Clase anidada para adaptar datos al CardView y RecyclerView
    public class AdapterListItem extends RecyclerView.Adapter<AdapterListItem.EventoViewHolder> {
        //Creacion de lista de items
        private List<Item_Evento> items;

        public class EventoViewHolder extends RecyclerView.ViewHolder{
            // Campos respectivos de un item
            private CircleImageView imagen;
            private TextView nombre;
            private TextView mensaje;

            public EventoViewHolder(View v) {
                //Inicilizando WidGets del CardView (item_evento)
                super(v);
                imagen = (CircleImageView) v.findViewById(R.id.CircleImgCard);
                nombre = (TextView) v.findViewById(R.id.txtNombreCard);
                mensaje = (TextView) v.findViewById(R.id.txtMensajeCard);
            }
        }

        public AdapterListItem(List<Item_Evento> items) {
            //Igualando datos de la lista
            this.items = items;
        }

        @Override
        public int getItemCount() {
            //Sacando tamaño de la lista
            return items.size();
        }

        @Override
        public AdapterListItem.EventoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            //Inflando Layout de CardView (item_evento)
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_evento, viewGroup, false);
            return new AdapterListItem.EventoViewHolder(v);
        }

        @Override
        public void onBindViewHolder(AdapterListItem.EventoViewHolder viewHolder, int i) {
            //Agregando los datos al CardView (item_evento)
            viewHolder.imagen.setImageBitmap(items.get(i).getImg());
            viewHolder.nombre.setText(items.get(i).getNombre());
            viewHolder.mensaje.setText(items.get(i).getMensaje());
        }
    }

}
