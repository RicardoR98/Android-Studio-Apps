package com.orbita.innovacion.ejemplo13fragmentos.fragmentos;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.orbita.innovacion.ejemplo13fragmentos.R;

import java.util.concurrent.ConcurrentLinkedDeque;


public class Agregar extends Fragment implements Eliminar.CallBackFragment{

    private EditText name, lastname, contra;
    private Button button, button2;
    private CallBackActivity callBackActivity;

    //Este metodo se ejecuta automaticamente cuando se ejecua el constrictor
    // es importante porque aqui inflamos en layout y manipulamos los widgets
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_agregar, container, false);

        name = (EditText) v.findViewById(R.id.edt_nombre);
        lastname = (EditText) v.findViewById(R.id.edt_apellido);
        contra = (EditText) v.findViewById(R.id.edt_password);
        button = (Button) v.findViewById(R.id.btn_Aceptar);
        button2 = (Button) v.findViewById(R.id.btn_Fragment);

        //button.setEnabled(false);
        final Bundle bundle = getArguments();
        if(bundle != null){
            name.setText(bundle.getString("name"));
            lastname.setText(bundle.getString("lastname"));
            contra.setText(bundle.getString("password"));
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Eliminar el fragmento
                getActivity()
                        .getFragmentManager()
                        .beginTransaction()
                        .remove(Agregar.this)
                        .commit();

                callBackActivity.getActivity(
                        name.getText().toString(),
                        lastname.getText().toString(),
                        contra.getText().toString()
                );
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.container2, new Eliminar()).commit();

                button2.setEnabled(false);
            }
        });

        return v;
    }

    public void recivir(String nombre, String lastName, String password){
        name.setText(nombre);
        lastname.setText(lastName);
        contra.setText(password);
    }

    //Se ejecuta cuando cargamos el layout
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /*@Override
    public void getFragment(String nombre, String lastName, String password) {
        name.setText(nombre);
        lastname.setText(lastName);
        contra.setText(password);
    }*/

    public interface CallBackActivity{
        public void getActivity(String nombre, String lastName, String password);
    }

    //Genera el procso de CallBack el cual es impresindiblle si queremos regresarun valor
    // a la actividad que lo esta llamando
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            //UpCasting
            callBackActivity = (CallBackActivity)getActivity();
        }catch (ClassCastException e){
            e.printStackTrace();
        }
    }

    @Override
    public void getFragment(String nombre, String lastName, String password) {
        name.setText(null);
        lastname.setText(null);
        contra.setText(null);

        name.setText(nombre);
        lastname.setText(lastName);
        contra.setText(password);

        button.setEnabled(true);
        button2.setEnabled(true);
    }

}
